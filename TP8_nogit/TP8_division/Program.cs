﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP8_division
{
     public class Program
    {
        public class MultipleDivision
        {
            private List<int> values;
            public MultipleDivision(List<int> values)
            {
                this.values = values;
            }

            public  double Division(int index_dividende, int index_diviseur)
            {
                
                double quotient;
                quotient = this.values[index_dividende] / this.values[index_diviseur];
                return quotient;
            }
        }

        static void Main(string[] args)
        {
            List<int> values = new List<int> { 17, 12, 15, 38, 29, 157, 89, -22, 0, 5 };
            MultipleDivision mltd = new MultipleDivision(values);
            int dividende, diviseur;
            string dividende_s, diviseur_s;
            Console.WriteLine("Saisir valeur");
            dividende_s = Console.ReadLine();
            int.TryParse(dividende_s, out dividende);
            diviseur_s = Console.ReadLine();
            int.TryParse(diviseur_s, out diviseur);
            Console.WriteLine(mltd.Division(dividende, diviseur));

            Console.Read();
        }
        
    }
}
