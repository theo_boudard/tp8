﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP8
{
    class Program
    {

        public static double RacineCarree(double valeur)
        {
            if (valeur <= 0)
                throw new ArgumentOutOfRangeException("valeur", "Le paramètre doit être positif");
            return Math.Sqrt(valeur);
        }


        static void Main(string[] args)
        {
            try
            {
                string user_value = "20";
                int value = int.Parse(user_value);
                Console.WriteLine("This line will never be executed");
            }
            catch (Exception)
            {
                Console.WriteLine("Error during user value conversion.");
            }

            Console.WriteLine(RacineCarree(-42));
            Console.Read();
        }

        
    }
    
}
